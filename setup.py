from __future__ import print_function
from setuptools import setup, find_packages
import sys

setup(
    name="structured_log_json",
    version="0.1.0",
    author="Qiao.putty",  #作者名字
    author_email="qiaowei0077@126.com",
    description="Python structured event expression in json log.",
    license="LICENSE",
    url="",  #github地址或其他地址
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Development Status :: 3 - Alpha",
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: Chinese (Simplified)',
        'Operating System :: MacOS',
        'Operating System :: Microsoft',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Topic :: System :: Logging',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: 3.12'
    ],
    python_requires = '>=3.6.*',
    platforms = 'any',
    install_requires=[
             #所需要包的版本号
    ],
    zip_safe=True,
)
