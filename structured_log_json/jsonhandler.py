from logging.handlers  import BaseRotatingHandler
import os
import re
import time



class JsonRotatingFileHandler(BaseRotatingHandler):
    """
    Handler for logging to a set of files, which switches from one file
    to the next when the current file reaches a certain size.
    """
    def __init__(self, filename, mode='a', maxBytes=0, backupCount=0,
                 encoding=None, delay=False, errors=None):
        """
        Open the specified file and use it as the stream for logging.

        By default, the file grows indefinitely. You can specify particular
        values of maxBytes and backupCount to allow the file to rollover at
        a predetermined size.

        Rollover occurs whenever the current log file is nearly maxBytes in
        length. If backupCount is >= 1, the system will successively create
        new files with the same pathname as the base file, but with extensions
        "arbitrament_20220101_0001.log", "arbitrament_20220101_0001.log" etc. appended to it.
        For example, with a backupCount of 5
        and a base file name of "arbitrament.log" log at 2022.01.01, you would get "arbitrament_20220101_0000.log",
        "arbitrament_20220101_0001.log", "arbitrament_20220101_0002.log", ... through to "arbitrament_20220101_0005.log".
        The file being written to is always "arbitrament_20220101_0000.log" - when it gets filled up, it is closed
        and renamed to "arbitrament_20220101_0001.log", and if files "arbitrament_20220101_0001.log", "arbitrament_20220101_0002.log" etc.
        exist, then they are renamed to "arbitrament_20220101_0003.log", "arbitrament_20220101_0004.log" etc.
        respectively.

        If maxBytes is zero, rollover never occurs.
        """
        # If filename not end with"_\d{4}\d{2}\d{2}(\.\w+)?$" ;
        # filename need append date suffix
        if re.search("(_\d{4}\d{2}\d{2}(\.\w+)?$)", filename) is None:
            filename = self.__add_date_in_filename(filename)
        # split Filename's prefix and suffix
        self.suffixFilename = self.__get_suffixFilename_from_baseFilename(filename)
        self.prefixFilename = self.__get_prefixFilename_from_baseFilename(filename, self.suffixFilename)

        # If rotation/rollover is wanted, it doesn't make sense to use another
        # mode. If for example 'w' were specified, then if there were multiple
        # runs of the calling application, the logs from previous runs would be
        # lost if the 'w' is respected, because the log file would be truncated
        # on each run.
        if maxBytes > 0:
            mode = 'a'
        BaseRotatingHandler.__init__(self, filename, mode, encoding=encoding,
                                     delay=delay, errors=errors)
        self.maxBytes = maxBytes
        self.backupCount = backupCount

    def __get_suffixFilename_from_baseFilename(self, filename, suffix:str = '.log'):
        suffixFilename = ''
        suffixPos = filename.rfind(suffix)
        if suffixPos != -1:
            suffixFilename = filename[suffixPos::]
        return suffixFilename

    def __get_prefixFilename_from_baseFilename(self, filename, suffixFilename:str = '.log'):
        prefixFilename = ''
        suffixPos = filename.rfind(suffixFilename)
        if suffixPos != -1:
            prefixFilename = filename[:suffixPos]
        return prefixFilename

    def __add_date_in_filename(self, filename)-> str:
        """
        filename splice date, create new filename : XXXX_YYYYMMDD_0000.log
        :return: newFilename->str
        """
        #1. get current time, and formate date
        dateStr = time.strftime('%Y%m%d', time.localtime(time.time()))
        #2. filename splice date
        prefixFilename = self.__get_prefixFilename_from_baseFilename(filename)
        suffixFilename = self.__get_suffixFilename_from_baseFilename(filename)
        newFilename = prefixFilename + '_' + dateStr + suffixFilename

        return newFilename

    def doRollover(self):
        """
        Do a rollover, as described in __init__().
        """
        if self.stream:
            self.stream.close()
            self.stream = None
        # 1. baseFilename splice date
        #baseFilename,  dateStr  = self.__splice_date_suffix()
        # 2.  switches from one file to the next when the current file reaches a certain size.
        if self.backupCount > 0:
            for i in range(self.backupCount - 1, 0, -1):
                sfn = self.rotation_filename("{}_{:04d}{}".format(self.prefixFilename,
                                                                      i,
                                                                      self.suffixFilename))
                dfn = self.rotation_filename("{}_{:04d}{}".format(self.prefixFilename,
                                                                              i + 1,
                                                                              self.suffixFilename))
                if os.path.exists(sfn):
                    if os.path.exists(dfn):
                        os.remove(dfn)
                    os.rename(sfn, dfn)
            dfn = self.rotation_filename("{}_{:04d}{}".format(self.prefixFilename,
                                                                    1,
                                                                    self.suffixFilename))
            if os.path.exists(dfn):
                os.remove(dfn)
            self.rotate(self.baseFilename, dfn)
        if not self.delay:
            self.stream = self._open()

    def shouldRollover(self, record):
        """
        Determine if rollover should occur.

        Basically, see if the supplied record would cause the file to exceed
        the size limit we have.
        """
        if self.stream is None:                 # delay was set...
            self.stream = self._open()
        if self.maxBytes > 0:                   # are we rolling over?
            msg = "%s\n" % self.format(record)
            self.stream.seek(0, 2)  #due to non-posix-compliant Windows feature
            if self.stream.tell() + len(msg) >= self.maxBytes:
                return 1
        return 0